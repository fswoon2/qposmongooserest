var mongoose = require('mongoose');
var Schema = mongoose.Schema;
require('mongoose-currency').loadType(mongoose);
var Currency = mongoose.Types.Currency;

var promoSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
     image: {
        type: String,
        required: true,
        unique: true
    },
     label: {
        type: String,
         default: null,
        required: false,
        unique: true
    },
     price: {
        type: Currency,
        required: true,
        unique: true
    },
    description: {
        type: String,
        required: true
    },
     featured: {
        type: Boolean,
        default:false
    }
});
var Promotions = mongoose.model('Promo', promoSchema);

// make this available to our Node applications
module.exports = Promotions;