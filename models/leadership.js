var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var leaderSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
     image: {
        type: String,
        required: true,
        unique: true
    },
     designation: {
        type: String,
        required: false,
        unique: true
    },
     abbr: {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String,
        required: true
    },
     featured: {
        type: Boolean,
        default:false
    }
});
var Leadership = mongoose.model('Leader', leaderSchema);

// make this available to our Node applications
module.exports = Leadership;