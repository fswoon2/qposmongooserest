var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var Carts = require('../models/carts')
var Verify = require('./verify');

var cartRouter = express.Router();
cartRouter.use(bodyParser.json());

cartRouter.route('/')

.get(Verify.verifyOrdinaryUser, function (req, res, next) {
    Carts.findOne({postedBy: req.decoded.data._id})
        .populate('dishes')
    .populate('postedBy')
        .exec(function (err, Cart) {
        if (err) throw err;
        res.json(Cart);
    });
})


.delete(Verify.verifyOrdinaryUser, function (req, res, next) {
    Carts.remove({postedBy: req.decoded.data._id}, function (err, resp) {
        if (err) throw err;
        res.json(resp);
    });
})

cartRouter.route('/:dishId')

.post(Verify.verifyOrdinaryUser, function (req, res, next) {
	    Carts.findOneAndUpdate(
	    {postedBy: req.decoded.data._id},
	   {$addToSet:{'dishes':req.params.dishId}},
	    {upsert:true, new:true} , function (err, Cart) {
	          if (err) throw err;
	          console.log('You added a dish to your Carts!');
	          res.json(Cart);
        });
})

.delete(Verify.verifyOrdinaryUser, function (req, res, next) {
        Carts.findOne({postedBy:req.decoded.data._id},
        function (err, Cart) {
            if (err) throw err;

            var index = Cart.dishes.indexOf(req.params.dishId);

            Cart.dishes.splice(index, 1);
            Cart.save(function (err, dish) {
                if (err) throw err;
                res.json(dish);
            });
        });
});

module.exports = cartRouter;