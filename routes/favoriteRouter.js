var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var Favorites = require('../models/favorites')
var Verify = require('./verify');

var favoriteRouter = express.Router();
favoriteRouter.use(bodyParser.json());

favoriteRouter.route('/')

.get(Verify.verifyOrdinaryUser, function (req, res, next) {
    Favorites.findOne({postedBy: req.decoded.data._id})
        .populate('dishes')
    .populate('postedBy')
        .exec(function (err, Favorite) {
        if (err) throw err;
        res.json(Favorite);
    });
})


.delete(Verify.verifyOrdinaryUser, function (req, res, next) {
    Favorites.remove({postedBy: req.decoded.data._id}, function (err, resp) {
        if (err) throw err;
        res.json(resp);
    });
})

favoriteRouter.route('/:dishId')

.post(Verify.verifyOrdinaryUser, function (req, res, next) {
	    Favorites.findOneAndUpdate(
	    {postedBy: req.decoded.data._id},
	   {$addToSet:{'dishes':req.params.dishId}},
	    {upsert:true, new:true} , function (err, Favorite) {
	          if (err) throw err;
	          console.log('You added a dish to your Favorites!');
	          res.json(Favorite);
        });
})

.delete(Verify.verifyOrdinaryUser, function (req, res, next) {
        Favorites.findOne({postedBy:req.decoded.data._id},
        function (err, Favorite) {
            if (err) throw err;

            var index = Favorite.dishes.indexOf(req.params.dishId);

            Favorite.dishes.splice(index, 1);
            Favorite.save(function (err, dish) {
                if (err) throw err;
                res.json(dish);
            });
        });
});

module.exports = favoriteRouter;